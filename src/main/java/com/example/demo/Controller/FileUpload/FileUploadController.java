package com.example.demo.Controller.FileUpload;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
public class FileUploadController {

    @PostMapping("/upload")
    public String upLoadFile(@RequestParam("file") MultipartFile file){
        File uploadedFile = new File("\\D:\\LEAPHENG-DOCUMET\\HRD\\Spring\\Spring Project\\HOMEWORK\\src\\main\\java\\com\\example\\demo\\Image\\"+file.getOriginalFilename());

        try {
            uploadedFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "localhost:8080/image/"+uploadedFile.getName();
    }
}
