package com.example.demo.Repository;

import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Pagination.Pagination;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

//    @Select("SELECT * FROM tb_categories")
//    List<CategoryDto> findAll();

    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    boolean delete(int id);

    @Insert("INSERT INTO tb_categories (title) VALUES (#{title})")
    boolean insert(CategoryDto categoryDto);

    @Update("UPDATE tb_categories SET title=#{categoryDto.title} WHERE id=#{id}")
    boolean update(int id,CategoryDto categoryDto);


    @Select("SELECT * FROM tb_categories ORDER BY id ASC LIMIT #{pagination.limit} OFFSET #{pagination.offset}")
    List<CategoryDto> getAllCategory(@Param("pagination") Pagination pagination);

    @Select("SELECT COUNT(id) FROM tb_categories")
    int coutAllCategories();
}
