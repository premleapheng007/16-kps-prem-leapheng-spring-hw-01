package com.example.demo.Controller.Category;

import com.example.demo.Model.BaseApiRespone;
import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Model.Request.BookRequestModel;
import com.example.demo.Model.Request.CategoryRequestModel;
import com.example.demo.Model.Respone.BookResponeModel;
import com.example.demo.Model.Respone.CategoryResponeModel;
import com.example.demo.Pagination.Pagination;
import com.example.demo.Service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("/api")
public class CategoryController {
    public CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

//    @GetMapping("/categories")
//    ResponseEntity<BaseApiRespone<List<CategoryDto>>> findAll(){
//        BaseApiRespone<List<CategoryDto>> respone=new BaseApiRespone<>();
//        respone.setMessage("Have already find all book");
//        respone.setHttpStatus(HttpStatus.OK);
//        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
//        respone.setData(categoryService.findAll());
//        return new ResponseEntity <>(respone, HttpStatus.OK);
//    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        return new ResponseEntity <>(categoryService.delete(id),HttpStatus.OK);
    }

    @PostMapping("/category")
    public ResponseEntity<BaseApiRespone<CategoryResponeModel>> insert(@RequestBody CategoryRequestModel requestModel){
        ModelMapper modelMapper=new ModelMapper();
        BaseApiRespone<CategoryResponeModel> respone=new BaseApiRespone <>();
        CategoryDto categoryDto=modelMapper.map(requestModel,CategoryDto.class);
        System.out.println( categoryService.insert(categoryDto));
        CategoryResponeModel responeModel=modelMapper.map(requestModel, (Type) CategoryResponeModel.class);
        respone.setMessage("YOU HAVE ADD SUCCESSFULLY!");
        respone.setHttpStatus(HttpStatus.CREATED);
        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        respone.setData(responeModel);
        return new ResponseEntity <>(respone,HttpStatus.CREATED);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiRespone<CategoryResponeModel>> update(@PathVariable int id,@RequestBody CategoryRequestModel requestModel){
        ModelMapper modelMapper=new ModelMapper();
        BaseApiRespone<CategoryResponeModel> respone=new BaseApiRespone <>();
        CategoryDto dto=modelMapper.map(requestModel,CategoryDto.class);
        CategoryResponeModel responeModel=modelMapper.map(categoryService.update(id,dto),CategoryResponeModel.class);
        respone.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        respone.setHttpStatus(HttpStatus.OK);
        respone.setData(responeModel);
        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(respone);
    }

    @RequestMapping (value="/categories" ,method=RequestMethod.GET )
    public Map<String, Object> getAllCategory(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                          @RequestParam(value = "limit", required = false, defaultValue = "4") int limit){
        Pagination pagination = new Pagination(page, limit);
        pagination.setPage(page);
        pagination.setLimit(limit);

        pagination.setTotalCount(categoryService.countAllCategory());
        pagination.setTotalPages(pagination.getTotalPages());
        Map<String, Object> result = new HashMap<>();
        List<CategoryDto> categoryDtos = categoryService.getAll(pagination);
        if(categoryDtos.isEmpty()){
            result.put("Message", "No data in Database");
        }else {
            result.put("Message", "Get Data Successful");
        }
        result.put("Pagination", pagination);
        result.put("Data", categoryDtos);
        return result;
    }

}
