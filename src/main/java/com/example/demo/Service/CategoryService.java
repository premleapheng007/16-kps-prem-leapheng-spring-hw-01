package com.example.demo.Service;

import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Pagination.Pagination;

import java.util.List;

public interface CategoryService {

//    List<CategoryDto> findAll();

    String delete(int id);

    CategoryDto insert(CategoryDto dto);

    CategoryDto update(int id, CategoryDto dto);

    List<CategoryDto> getAll(Pagination pagination);

    int countAllCategory();

}
