package com.example.demo.Service;

import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Pagination.Pagination;
import com.example.demo.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService{

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

//    @Override
//    public List<CategoryDto> findAll() {
//        return categoryRepository.findAll();
//    }

    @Override
    public String delete(int id) {
        boolean isDelete=categoryRepository.delete(id);
        if(isDelete){
            return "YOU HAVE DELETE SUCCESSFULLY!";
        }else {
            return "YOU CAN NOT DELETE!";
        }
    }

    @Override
    public CategoryDto insert(CategoryDto dto) {
        boolean isInserted = categoryRepository.insert(dto);
        if (isInserted) {
            return dto;
        } else {
            return null;
        }
    }

    @Override
    public CategoryDto update(int id, CategoryDto dto) {
        boolean isUpdated = categoryRepository.update(id, dto);
        if (isUpdated) {
            return dto;
        } else {
            return null;
        }
    }

    @Override
    public List<CategoryDto> getAll(Pagination pagination) {
        return categoryRepository.getAllCategory(pagination);
    }

    @Override
    public int countAllCategory() {
        return categoryRepository.coutAllCategories();
    }


}
